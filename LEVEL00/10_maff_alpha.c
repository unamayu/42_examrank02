/**
Assignment name  : maff_alpha
Expected files   : maff_alpha.c
Allowed functions: write
--------------------------------------------------------------------------------

Write a program that displays the alphabet, with even letters in uppercase, and
odd letters in lowercase, followed by a newline.

Example:

$> ./maff_alpha | cat -e
aBcDeFgHiJkLmNoPqRsTuVwXyZ$
 */

#include <unistd.h>
int main()
{
	int i = 0;
	char c = 'a';
	char d;

	while (i<26)
	{
		d = c+i ;
		write(1,&d,1);
		d = c +i +1 -32;
		write(1,&d,1);
		i++;
		i++;
	}
}