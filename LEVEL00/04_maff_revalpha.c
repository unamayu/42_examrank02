/**
Assignment name  : maff_revalpha
Expected files   : maff_revalpha.c
Allowed functions: write
--------------------------------------------------------------------------------

Write a program that displays the alphabet in reverse, with even letters in
uppercase, and odd letters in lowercase, followed by a newline.

Example:

$> ./maff_revalpha | cat -e
zYxWvUtSrQpOnMlKjIhGfEdCbA$
 */

#include <unistd.h>
int main ()
{
	write(1, "zYxWvUtSrQpOnMlKjIhGfEdCbA", 26);
	write(1, "\n", 1);
}