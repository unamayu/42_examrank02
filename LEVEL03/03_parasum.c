/*
Assignment name  : paramsum
Expected files   : paramsum.c
Allowed functions: write
--------------------------------------------------------------------------------

Write a program that displays the number of arguments passed to it, followed by
a newline.

If there are no arguments, just display a 0 followed by a newline.

Example:

$>./paramsum 1 2 3 5 7 24
6
$>./paramsum 6 12 24 | cat -e
3$
$>./paramsum | cat -e
0$
$>
*/
#include <unistd.h>
void ft_putnbr(int num, int base)
{
	char set[16]="0123456789abcdef";

	if (num/base >0)
		ft_putnbr(num/base, base);
	write(1, &set[num % base], 1);
}
int		main(int ac, char **av)
{
    av = 0;
    ft_putnbr(ac - 1, 10 );
    write(1, "\n", 1);
}