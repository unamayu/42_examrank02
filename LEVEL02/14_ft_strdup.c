/**
Assignment name  : ft_strdup
Expected files   : ft_strdup.c
Allowed functions: malloc
--------------------------------------------------------------------------------

Reproduce the behavior of the function strdup (man strdup).

Your function must be declared as follows:

char    *ft_strdup(char *src);
 */


#include <string.h>
#include <stdlib.h>

char    *ft_strdup(char *src)
{
	char *str;
	int len;
	int index;
	
	len = 0;
	index = 0;
	if (src == NULL)
		return (NULL);
	while (src[len])
		len++;
	str = (char *)malloc(sizeof((char) len + 1));
	if (str == NULL)
		return (NULL);
	while (src[index])
	{
		str[index] = src[index];
		index++;
	}
	str[index] = '\0';
	return ((char *)str);
}