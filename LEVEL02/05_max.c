/**
Assignment name  : max
Expected files   : max.c
Allowed functions: 
--------------------------------------------------------------------------------

Write the following function:

int		max(int* tab, unsigned int len);

The first parameter is an array of int, the second is the number of elements in
the array.

The function returns the largest number found in the array.

If the array is empty, the function returns 0.
 */

#include <stdio.h>
#include <stdlib.h>
int		max(int* tab, unsigned int len)
{
	int max;
	int i = 1;
	if (len > 0)
	{
		max = tab[0];
		while (i < len)
		{
			if (tab[i] > max)
				max = tab[i];
			i++;
		}
		return (max);
	}
	return (0);


}
int main()
{
	int *tab;

	tab = (int *)malloc(10*sizeof(int));

	tab[0] = 10;
	tab[1] = 10;
	tab[2] = 10;
	tab[3] = 10;
	tab[4] = 10;
	tab[5] = 10;
	tab[6] = 102;
	tab[7] = 10;
	tab[8] = 10;
	tab[9] = 10;

	printf("\n el mayor es %d", max(tab, 10));

}