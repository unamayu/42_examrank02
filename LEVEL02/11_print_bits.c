/**
Assignment name  : print_bits
Expected files   : print_bits.c
Allowed functions: write
--------------------------------------------------------------------------------

Write a function that takes a byte, and prints it in binary WITHOUT A NEWLINE
AT THE END.

Your function must be declared as follows:

void	print_bits(unsigned char octet);

Example, if you pass 2 to print_bits, it will print "00000010"
 */

#include <stdio.h>
#include <unistd.h>
void	print_bits(unsigned char octet)
{
	int numero;
	int base;

	base = 128;
	numero = octet;
	while (base != 0)
	{
		if (base <= numero)
		{
			write(1, "1", 1);
			numero = numero % base;
		}
		else
			write(1, "0", 1);
		
		base = base / 2;
	}
}

int main()
{
	print_bits(128);


}