/**
Assignment name  : reverse_bits
Expected files   : reverse_bits.c
Allowed functions:
--------------------------------------------------------------------------------

Write a function that takes a byte, reverses it, bit by bit (like the
example) and returns the result.

Your function must be declared as follows:

unsigned char	reverse_bits(unsigned char octet);

Example:

  1 byte
_____________
 0010  0110
	 ||
	 \/
 0110  0100
 */

unsigned char	reverse_bits(unsigned char octet)
{ 
	return ((((octet >> 0) & 1) << 7) |
			(((octet >> 1) & 1) << 6) |
			(((octet >> 2) & 1) << 5) |
			(((octet >> 3) & 1) << 4) |
			(((octet >> 4) & 1) << 3) |
			(((octet >> 5) & 1) << 2) |
			(((octet >> 6) & 1) << 1) |
			(((octet >> 7) & 1) << 0));
}
unsigned char	reverse_bits(unsigned char octet)
{
	unsigned char	res;
	int		count;

	res = 0;
	count = 8;
	while (count)
	{
		res = res * 2 + (unsigned char)(octet % 2);
		octet = octet / 2;
		count--;
	}
	return (res);
}
/*
#include <unistd.h>
unsigned char print_bits (unsigned char octet)
{
	int i = 128;
	while (i)
	{
		if (octet / i)
		{
			write(1, "1", 1);
			octet -= i;
		}
		else if (!(octet / i))
		{
			write(1, "0", 1);
		}
		i = i / 2;
	}
	return (octet);
}

#include <stdio.h>
int main ()
{	int n = 130;
	char octet = reverse_bits(n);
	print_bits(n);
	//print_bits(128);
   printf("\n");	
	//printf("%c", reverse_bits(1));
	//printf("\n");
   	print_bits(octet);	
	return (0);
}
*/
