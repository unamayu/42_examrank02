/**
Assignment name  : ft_strrev
Expected files   : ft_strrev.c
Allowed functions: 
--------------------------------------------------------------------------------

Write a function that reverses (in-place) a string.

It must return its parameter.

Your function must be declared as follows:

char    *ft_strrev(char *str);
 */

#include <stdio.h>

char	*ft_strrev(char *str)
{
	int	len;
	int	i;
	char	tmp;

	len = 0;
	i = 0;
	while (str[len]!='\0')
		len++;
	len --;
	while (i < (len/2))
	{
		//printf("\n %d %c %c", i,str[i], str[len-i]);
		tmp = str[i];
		str[i] = str[len-i];
		str[len-i] = tmp;
		
		i++;
		
	}
	//str[i] = '\0';
	return (str);
}
int	main(void)
{
	char s[] = "Hello World";
	ft_strrev(s);
	printf("%s\n", s);
	return (0);
}