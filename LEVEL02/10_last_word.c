#include <stdio.h>
#include <unistd.h>

/** pasqualerossi

 * ./last_word " FOR PONY"
 */
int		main(int argc, char **argv)
{
	int		i;
	int start;

	i = 0;
	start = 0;
	if (argc == 2)
	{
		while (argv[1][i+1]) //IMPORTANE EL I+1 //BUSCAMOS ' ' seguido de algo
		{
			if ((argv[1][i] == ' ' || argv[1][i]=='\t') && argv[1][i+1]!=' ' && argv[1][i+1]!='\t')
				start = i;
			i++;
		}
		printf("\n%d", start);
		i = start;
		while (argv[1][i] == ' ' || argv[1][i] == '\t')
			i++;
		while (argv[1][i] != ' ' && argv[1][i] != '\0' && argv[1][i] != '\t')
		{
			write (1, &argv[1][i], 1);
			i++;
		}
	}
	write(1, "\n", 1);
	return (0);
}