/**
Assignment name	: ft_strspn
Expected files	: ft_strspn.c
Allowed functions: None
---------------------------------------------------------------

Reproduce exactly the behavior of the strspn function 
(man strspn).

The function should be prototyped as follows:

size_t  ft_strspn(const char *s, const char *accept);


STRSPN(3)                BSD Library Functions Manual                STRSPN(3)

NAME
     strspn, strcspn -- span a string

LIBRARY
     Standard C Library (libc, -lc)

SYNOPSIS
     #include <string.h>

     size_t
     strspn(const char *s, const char *charset);

     size_t
     strcspn(const char *s, const char *charset);

DESCRIPTION
     The strspn() function spans the initial part of the null-terminated string s as long as the characters from s occur in the null-termi-
     nated string charset.  In other words, it computes the string array index of the first character of s which is not in charset, else the
     index of the first null character.


RETURN VALUES
     The strspn() and strcspn() functions return the number of characters spanned.

SEE ALSO
     memchr(3), strchr(3), strpbrk(3), strrchr(3), strsep(3), strstr(3), strtok(3), wcsspn(3)

STANDARDS
     The strspn() and strcspn() functions conform to ISO/IEC 9899:1990 (``ISO C90'').

BSD                              May 24, 2014                              BSD

*/

#include <stdlib.h>

char	*ft_strchr(const char *s, int c)
{
	char	*str;

	str = (char *)s;
	while (*str != c)
	{
		if (*str == '\0')
		{
			return (NULL);
		}
		str++;
	}
	return (str);
}

size_t	ft_strspn(const char *s1, const char *s2)
{
	size_t		count;

	count = 0;
	while (*s1)
	{
		if (ft_strchr(s2, *s1))
			count++;
		else
			break;
		s1++;
	}
	return (count);
}
