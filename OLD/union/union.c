/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   union.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: xamayuel <xamayuel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/06 12:22:32 by xamayuel          #+#    #+#             */
/*   Updated: 2022/03/06 12:32:42 by xamayuel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int main (int argn, char *argv[])
{
    char used[255];
    int i;

    if (argn == 3) // solo hacemos si 3 argumentos
    {
        // INICIALIZAMOS USED
        i = 0;      
        while (i < 255)
        {
            used[i] = 0;
            i++;
        }
        // RECORREMOS EL PRIMER ARGUMENTO
        i = 0;
        while (argv[1][i])
        {
            used[(unsigned char)argv[1][i]] ++;
            if (used[(unsigned char)argv[1][i]]== 1)
                write(1, &argv[1][i], 1);
            i++;
        }
        // RECORREMOS EL SEGUNDO ARGUMENTO
        i = 0;
        while (argv[2][i])
        {
            used[(unsigned char)argv[2][i]] ++;
            if (used[(unsigned char)argv[2][i]]== 1)
                write(1, &argv[2][i], 1);
            i++;
        }

    }
    write(1, "\n", 1);
}