/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inter.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: xamayuel <xamayuel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/06 12:33:15 by xamayuel          #+#    #+#             */
/*   Updated: 2022/03/06 14:58:56 by xamayuel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int main (int argn, char *argv[])
{
    char used[255];
    int i;

    if (argn == 3)
    {
         // INCIALIZAMOS USED
         i = 0;
         while(i<255)
         {
             used[i]=0;
             i++;
         }
         // BUCLE SOBRE EL SEGUNDO ARGUMENTO

         i = 0;
         while (argv[2][i])
         {
             used[(unsigned char)argv[2][i]]=1;
             i++;
         }
         // BUCLE SOBRE EL PRIMERO
         i = 0;
         while (argv[1][i])
         {
             if (used[(unsigned char)argv[1][i]] == 1)
             {
                write(1, &argv[1][i],1 );
                used[(unsigned char)argv[1][i]]++;
             }
            i++;
         }
    }
   
    write(1, "\n", 1);
}