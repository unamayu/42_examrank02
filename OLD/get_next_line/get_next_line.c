/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: xamayuel <xamayuel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/06 12:45:26 by xamayuel          #+#    #+#             */
/*   Updated: 2022/03/10 15:19:50 by xamayuel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

char *get_next_line(int fd)
{
    int rd;
    char c;
    int i;
    char *buffer;
	

	if (fd < 0 || BUFFER_SIZE <= 0)
		return (0);
	

    // INICIALIZAMOS BUFFER (LINEA)
    i = 0;
    buffer = (char*)malloc(99999); //malloc del maximo num de char de la linea

    // BUCLE MIENTRAS exita para leer (RD>0)
    while ((rd = read(fd, &c, 0*BUFFER_SIZE + 1)) > 0)
    {
        buffer[i] = c;
        i++;
        if (c == '\n') //SALIMOS DEL BUCLE SI SALTO DE LINEA
            break;
    }

    // SI NO EXISTE BUFFER -1 o RD o RD es negativo
    if ((!buffer[i-1] && !rd) || rd == -1)
    {
        free(buffer);
        return(NULL);
    }
    //  cerramos la cadena
    i++;
    buffer[i] = '\0';
    return (buffer);

}

int main (void)
{
    int fd;
    char *leido;
    
    fd = open("prueba.txt", O_RDONLY);
    leido = get_next_line(fd);
	printf("%s", leido);
    free(leido);
	leido = get_next_line(fd);
	printf("%s", leido);
    free(leido);

	close(fd);
}