/*
Assignment name  : brainfuck
Expected files   : *.c, *.h
Allowed functions: write, malloc, free
--------------------------------------------------------------------------------

Write a Brainfuck interpreter program.
The source code will be given as first parameter.
The code will always be valid, with no more than 4096 operations.
Brainfuck is a minimalist language. It consists of an array of bytes 
(in our case, let's say 2048 bytes) initialized to zero, 
and a pointer to its first byte.

Every operator consists of a single character :
- '>' increment the pointer ;
- '<' decrement the pointer ;
- '+' increment the pointed byte ;
- '-' decrement the pointed byte ;
- '.' print the pointed byte on standard output ;
- '[' go to the matching ']' if the pointed byte is 0 (while start) ;
- ']' go to the matching '[' if the pointed byte is not 0 (while end).

Any other character is a comment.

Examples:

$>./brainfuck "++++++++++[>+++++++>++++++++++>+++>+<<<<-]
>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>." | cat -e
Hello World!$
$>./brainfuck "+++++[>++++[>++++H>+++++i<<-]>>>++\n<<<<-]>>--------.>+++++.>." | cat -e
Hi$
$>./brainfuck | cat -e
$
*/

#include <unistd.h>

char	g_bytarr[2048];
char	*g_ptr;
char	*g_arg;

void	issqr()
{
	int br;

	br = 0;
	if (*g_arg == '[')
	{
		while (*g_arg)
		{
			if (*g_arg == '[')
				br++;
			else if (*g_arg == ']')
				br--;
			if (br == 0)
				return;
			g_arg++;
		}
	}
	else
	{
		while (*g_arg)
		{
			if (*g_arg == ']')
				br++;
			else if (*g_arg == '[')
				br--;
			if (br == 0)
				return;
			g_arg--;
		}
	}
}

int		main(int argc, char **argv)
{
	if (argc == 2)
	{
		g_arg = argv[1];
		g_ptr = g_bytarr;
		while (*g_arg )
		{
			if (*g_arg == '>')
				g_ptr++;
			else if (*g_arg == '<')
				g_ptr--;
			else if (*g_arg == '+')
				(*g_ptr)++;
			else if (*g_arg == '-')
				(*g_ptr)--;
			else if (*g_arg == '.')
				write(1, g_ptr, 1);
			else if (*g_arg == '[')
			{
				if (*g_ptr == 0)
					issqr();
			}
			else if (*g_arg == ']')
			{
				if (*g_ptr != 0)
					issqr();
			}
			g_arg++;
		}		
	}
	else
		write(1, "\n", 1);
	return (0);
}