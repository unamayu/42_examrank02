/*
Assignment name  : sort_int_tab
Expected files   : sort_int_tab.c
Allowed functions: 
--------------------------------------------------------------------------------

Write the following function:

void sort_int_tab(int *tab, unsigned int size);

It must sort (in-place) the 'tab' int array, that contains exactly 'size'
members, in ascending order.

Doubles must be preserved.

Input is always coherent.
*/
void	ft_swap(int *a, int *b)
{
	int temp;

	temp = *a;
	*a = *b;
	*b = temp;
}

void sort_int_tab(int *tab, unsigned int size)
{
	unsigned int index = 0;
	while (index + 1 <= size -1)
	{
		if (tab[index] == tab[index + 1])
			index++;
		else if (tab[index] > tab[index + 1])
		{
			ft_swap(&tab[index], &tab[index + 1]);
			index = 0;
		}
		else if (tab[index] < tab[index + 1])
			index++;
		else
			index++;
	}
}


#include <stdio.h>
int main ()
{
	int a = 5;
	int b = 1;
	
	printf("\nswap\n");
	ft_swap(&a, &b);
	printf("a = %i, b = %i", a, b);

	printf("\ntab\n");
	int tab[] = {19, 18, 5, -1, 8, 0, 2, -20, +50, 0};
	int index = 0;
	int size = 10;
	while (index < size)
	{
		printf("|%i| ", tab[index++]);
	}
	printf("\nnew\n");
	index = 0;
	sort_int_tab(tab, size);
	while (index <= size - 1)
	{
		printf("|%i| ", tab[index++]);
	}
	return (0);
}
