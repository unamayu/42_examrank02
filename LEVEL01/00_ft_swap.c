/*
Assignment name  : ft_swap
Expected files   : ft_swap.c
Allowed functions: 
--------------------------------------------------------------------------------

Write a function that swaps the contents of two integers the adresses of which
are passed as parameters.

Your function must be declared as follows:

void	ft_swap(int *a, int *b);
 */

#include <unistd.h>
#include <stdio.h>

void	ft_swap(int		*a, int		*b)
{
	int		tmp;

	tmp = *a;
	*a = *b;
	*b =tmp;
}

int main()
{
	int uno = 1;
	int dos = 2;

	printf("\n ANTES %d %d", uno, dos);
	ft_swap(&uno, &dos);
	printf("\n DESPUES %d %d", uno, dos);

}